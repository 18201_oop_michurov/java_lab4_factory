package ru.nsu.g.mmichurov.factory.ui.util;

public interface View<T extends Alterable> {

    void update();
}
