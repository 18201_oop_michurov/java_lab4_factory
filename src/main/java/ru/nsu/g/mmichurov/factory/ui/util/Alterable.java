package ru.nsu.g.mmichurov.factory.ui.util;

public interface Alterable {

    void setView(View<? extends Alterable> view);
}
